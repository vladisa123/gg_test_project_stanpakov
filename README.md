# Test project Stanpakov Vladislav


How to set up the project:
* Copy the project using:
```
  https://gitlab.com/vladisa123/gg_test_project_stanpakov.git
```

* Configure version of ELK stack in .env file
* Start-up the project via ansible:
```bash
  ansible-playbook elk_startup_playbook.yml
```